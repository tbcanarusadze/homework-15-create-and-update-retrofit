package com.example.homework15

import android.util.Log.d
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*


object HttpRequest {


    const val USERS = "users"
//    const val USERS2 = "users/2"
    private const val HTTP_200_OK = 200
    private const val HTTP_CREATED = 201
    private const val HTTP_BAD_REQUEST = 400
    private const val HTTP_NOT_FOUND = 404


    private var retrofit = Retrofit.Builder()
        .baseUrl("https://reqres.in/api/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .build()

    private var service = retrofit.create(ApiService::class.java)

    interface ApiService {

        @GET("{path}")
        fun getRequest(@Path("path") competitions: String): Call<String>

        @POST("{path}")
        @FormUrlEncoded
        fun postRequest(@Path("path") path: String, @FieldMap parameters: Map<String, String>): Call<String>

        @PUT("{path}")
        @FormUrlEncoded
        fun putRequest(@Path("path") path: String, @FieldMap parameters: Map<String, String>): Call<String>
    }

    fun getRequest(path: String, callback: CustomCallback) {
        val call = service.getRequest(path)
        call.enqueue(onCallback(callback, null))
    }

    fun putRequest(
        path: String,
        parameters: MutableMap<String, String>,
        callback: CustomCallback
    ) {
        val call = service.postRequest(path, parameters)
        call.enqueue(onCallback(callback, parameters))
    }


    fun postRequest(
        path: String,
        parameters: MutableMap<String, String>,
        callback: CustomCallback
    ) {
        val call = service.postRequest(path, parameters)
        call.enqueue(onCallback(callback, parameters))
    }

    private fun onCallback(callback: CustomCallback, parameters: MutableMap<String, String>?) =
        object : Callback<String> {
            override fun onFailure(call: Call<String>, t: Throwable) {
                d("onFailure", "${t.message}")
                callback.onFailure(t.message.toString(), "${t.message}")
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {

                val statusCode = response.code()
                if (statusCode == HTTP_200_OK || statusCode == HTTP_CREATED) {
                    callback.onResponse(response.body().toString(), "${response.body()}")
                    d("onResponse", "${response.body()}")
                } else if (statusCode == HTTP_BAD_REQUEST || statusCode == HTTP_NOT_FOUND) {
                    try {
                        val json = JSONObject(response.errorBody()!!.string())
                        parameters?.forEach {
                            if (json.has(it.key)) {
                                json.getString(it.key)
                                return
                            }
                        }
                        if (json.has("error")) {
                            d("error", json.getString("error"))
                            callback.onFailure(response.body().toString(), json.getString("error"))
                        }
                    } catch (e: JSONException) {

                    }


                }
            }
        }
}