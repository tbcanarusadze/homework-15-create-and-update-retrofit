package com.example.homework15

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.MenuView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_recyclerview_layout.*
import kotlinx.android.synthetic.main.item_recyclerview_layout.view.*

class RecyclerViewAdapter(private val items: MutableList<ItemModel>, private val itemOnClick: ItemOnClick) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_recyclerview_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        return holder.onBind()
    }

    override fun getItemCount() = items.size

    private lateinit var model: ItemModel

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),  View.OnClickListener {
        fun onBind() {
            model = items[adapterPosition]
            itemView.idTextView.text = model.id
            itemView.nameTextView.text = model.name
            itemView.jobTextView.text = model.job
            itemView.createdAtTextView.text = model.createdAt
            itemView.updateButton.setOnClickListener(this)

        }

        override fun onClick(v: View?) {
            itemOnClick.update(adapterPosition, itemView)
            itemView.updateButton.setBackgroundResource(R.color.colorPrimary)

        }
    }

    interface ItemOnClick {
        fun update(position:Int, itemView: View)
    }
}