package com.example.homework15

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework15.HttpRequest.USERS
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.item_recyclerview_layout.*

class CreateActivity : AppCompatActivity() {

    private val itemsList = mutableListOf<ItemModel>()
    private lateinit var adapter: RecyclerViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

    }

    private fun init() {
        createButton.setOnClickListener {
            create(USERS)
        }
        adapter = RecyclerViewAdapter(itemsList, object : RecyclerViewAdapter.ItemOnClick {
            @SuppressLint("SetTextI18n")
            override fun update(position: Int, itemView: View) {
                Toast.makeText(
                    applicationContext,
                    " now you can update information",
                    Toast.LENGTH_SHORT
                ).show()
                createButton.text = "update"
                modeTextView.text = "Update User"
                userLayout.setBackgroundResource(R.color.colorPrimary)
                createButton.setOnClickListener {
                    saveInfo(position)
                }
            }
        })
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

    }



    private fun create(path: String) {
        val parameters = mutableMapOf<String, String>()
        parameters["name"] = nameEditText.text.toString()
        parameters["job"] = jobEditText.text.toString()

        HttpRequest.postRequest(path, parameters, object : CustomCallback {
            override fun onFailure(response: String, message: String) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onResponse(response: String, message: String) {
                val itemModel = Gson().fromJson(response, ItemModel::class.java)
                itemsList.add(0, itemModel)
                adapter.notifyItemInserted(0)
            }
        })
    }

    private fun saveInfo(position: Int){
        val parameters = mutableMapOf<String, String>()
        val name = nameEditText.text.toString()
        val job = jobEditText.text.toString()
        val id = idTextView.text.toString()
        parameters["name"] = name
        parameters["job"] = job
//        აუ ვერ ვწვდები ამ მეორე რექუესტს, რაც არ უნდა ვქნა
        HttpRequest.putRequest("$USERS/$id",  parameters, object: CustomCallback{
            override fun onFailure(response: String, message: String) {
                Toast.makeText(applicationContext, "Error while loading page", Toast.LENGTH_SHORT)
                    .show()
            }

            @SuppressLint("SetTextI18n")
            override fun onResponse(response: String, message: String) {

                itemsList[position] = ItemModel(name, job, idTextView.text.toString())
                adapter.notifyItemChanged(position)
                Toast.makeText(applicationContext, "User Updated Successfully", Toast.LENGTH_SHORT)
                    .show()
                createButton.text= "create"
                createButton.setOnClickListener {
                    create(USERS)
                }
                userLayout.setBackgroundResource(R.color.colorWhite)
                modeTextView.text = "Create User"

            }
        })

    }

}



